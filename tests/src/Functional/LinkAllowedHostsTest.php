<?php

namespace Drupal\Tests\link_allowed_hosts\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Class to functional test in link allowed hosts module.
 */
class LinkAllowedHostsTest extends BrowserTestBase {

  /**
   * Administrative user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * {@inheritDoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritDoc}
   */
  protected static $modules = [
    'link_allowed_hosts',
    'link',
    'field',
    'field_ui',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->adminUser = $this->drupalCreateUser([], 'user', TRUE);
  }

  /**
   * Test basic functionalities of the module.
   */
  public function testAllowedHosts() {
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('/admin/config/people/accounts/fields/add-field');
    // Create a link field type.
    $link_field = [
      'new_storage_type' => 'link',
      'field_name' => 'allowed_hosts',
      'label' => 'allowed hosts',
    ];
    $this->submitForm($link_field, 'Save and continue');
    $this->submitForm([], 'Save field settings');
    // Define host path and unchecked Force HTTPS option.
    $field_host = [
      'allowed_hosts' => 'www.drupal.org ',
      'secure_hosts' => FALSE,
    ];
    $this->submitForm($field_host, 'Save settings');
    $this->drupalGet('/user/1/edit');
    // Define url.
    $edit = [
      'field_allowed_hosts[0][uri]' => '/www.drupal.org/user',
    ];
    $this->submitForm($edit, 'Save');

    $this->assertSession()->pageTextContains('The changes have been saved.');
    $this->drupalGet('/user/1/edit');
    // Test path in https format.
    $edit = [
      'field_allowed_hosts[0][uri]' => '/https://twitter.com/i/flow/login',
    ];
    $this->submitForm($edit, 'Save');
    $this->assertSession()->pageTextContains("The path '/https://twitter.com/i/flow/login' is invalid.");
    // It happens because the https format is disabled.
  }

}
