# Link Allowed Hosts

The Link Allowed Hosts module allows site builders to restrict the links entered
in a link field to contain only links to a configurable list of allowed hosts.
Let's say you have a link field where only links to Twitter should be entered.
Then go to this link fields edit screen and add only twitter.com to the list of
allowed hosts. Additionally you also can configure each link field to contain
only secure external links. The entered links then must start with https://.

For a full description of the module, visit the [project page][1].

Submit bug reports and feature suggestions, or track changes in the
[issue queue][2].

## Requirements

This module requires no modules outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see [Installing Drupal Modules][3].

## Configuration

After you added a link field to an entity, you can go to this field's edit
screen where you have an "Allowed hosts" fieldset with two possible settings:

* Hosts

  Specify hosts e.g. `drupal.org` or `www.drupal.org` or `*.drupal.org`. Enter
  one host per line.

* Force HTTPS

  Check this checkbox to allow only secure external links starting with
  https://.

## Maintainers

* Yannick - [yannickoo](https://www.drupal.org/u/yannickoo)
* Arthur Lorenz - [arthur_lorenz](https://www.drupal.org/u/arthur_lorenz)
* Norman Kämper-Leymann - [leymannx](https://www.drupal.org/u/leymannx)

## Supporting Organizations

* 1xINTERNET

  1xINTERNET is your Drupal agency in Germany, Spain and Iceland. Visit
  [1xinternet.de/en][4] for more information.

[1]: https://www.drupal.org/project/link_allowed_hosts

[2]: https://www.drupal.org/project/issues/link_allowed_hosts

[3]: https://www.drupal.org/docs/extending-drupal/installing-drupal-modules

[4]: https://www.1xinternet.de/en
