<?php

namespace Drupal\link_allowed_hosts\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Check that the submitted URL is starting with https://.
 *
 * @Constraint(
 *   id = "LinkSecureHost",
 *   label = @Translation("Link secure host", context = "Validation"),
 *   type = "string"
 * )
 */
class LinkSecureHostConstraint extends Constraint {

  public $message = 'The URL must start with https://.';

}
