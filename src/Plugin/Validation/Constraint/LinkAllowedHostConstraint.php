<?php

namespace Drupal\link_allowed_hosts\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Check that the submitted URL contains certain host names only.
 *
 * @Constraint(
 *   id = "LinkAllowedHost",
 *   label = @Translation("Link allowed host", context = "Validation"),
 *   type = "string"
 * )
 */
class LinkAllowedHostConstraint extends Constraint {

  public $message = 'The host %host is forbidden. Only the following are allowed: %allowed_hosts.';

  /**
   * The hosts option.
   *
   * @var string
   */
  public $hosts;

  /**
   * Return the hosts option as an array.
   *
   * @return array
   *   An associative array of allowed hosts.
   */
  public function getHosts() {
    $hosts = array_filter(array_map('trim', explode("\n", $this->hosts)));
    return array_combine($hosts, $hosts);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOption() {
    return 'hosts';
  }

  /**
   * {@inheritdoc}
   */
  public function getRequiredOptions() {
    return (array) $this->getDefaultOption();
  }

}
