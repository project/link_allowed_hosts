<?php

namespace Drupal\link_allowed_hosts\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Path\PathMatcherInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validate the LinkAllowedHost constraint.
 */
class LinkAllowedHostConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * The hosts option.
   *
   * @var string
   */
  public $hosts;

  /**
   * The path matcher.
   *
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected $pathMatcher;

  /**
   * LinkAllowedHostConstraintValidator constructor.
   *
   * @param \Drupal\Core\Path\PathMatcherInterface $path_matcher
   *   The path matcher.
   */
  public function __construct(PathMatcherInterface $path_matcher) {
    $this->pathMatcher = $path_matcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('path.matcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {
    if ($items->isEmpty()) {
      return NULL;
    }

    $allowed_hosts = $constraint->getHosts();
    /** @var \Drupal\link\Plugin\Field\FieldType\LinkItem $item */
    foreach ($items as $delta => $item) {
      $uri = $item->uri;

      // If no scheme set (in case of internal:, entity: etc.), exit early
      // and let Drupal handle.
      if (!filter_var($uri, FILTER_VALIDATE_URL)) {
        continue;
      }
      elseif ($item->isExternal()) {
        $url_host = parse_url($uri, PHP_URL_HOST);
        if (!$this->pathMatcher->matchPath($url_host, implode("\n", $allowed_hosts))) {
          $this->context->buildViolation($constraint->message)
            ->setParameter('%host', $url_host)
            ->setParameter('%allowed_hosts', implode(', ', $allowed_hosts))
            ->atPath($delta)
            ->addViolation();
        }
      }
    }
  }

}
