<?php

namespace Drupal\link_allowed_hosts\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validate the LinkSecureHost constraint.
 */
class LinkSecureHostConstraintValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {
    if ($items->isEmpty()) {
      return NULL;
    }

    /** @var \Drupal\link\Plugin\Field\FieldType\LinkItem $item */
    foreach ($items as $delta => $item) {
      $uri = $item->uri;

      // If no scheme set (in case of internal:, entity: etc.), exit early
      // and let Drupal handle.
      if (!filter_var($uri, FILTER_VALIDATE_URL)) {
        continue;
      }
      elseif (strpos($uri, 'https://') !== 0) {
        $this->context->buildViolation($constraint->message)
          ->setParameter('%uri', $uri)
          ->atPath($delta)
          ->addViolation();
      }
    }
  }

}
